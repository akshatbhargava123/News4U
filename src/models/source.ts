export class Source {
    id: number;
    name: string;
    description: string;
    url: string;
    category: string;
    language: string;
    country: string;
    urlsToLogos: any;
    sortBysAvailable: string[];
}