import { Component, ViewChild } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'MenuPage';
  @ViewChild('nav') nav;

  constructor(statusBar: StatusBar,
              splashScreen: SplashScreen,
              public platform: Platform,
              public alertCtrl: AlertController) {
    this.platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.platform.registerBackButtonAction(() => {
        if (this.nav.canGoBack()) {
          this.nav.pop();
        }
        else {
          this.confirmExit();
        }
      });
    });
  }

  confirmExit() {
    let exitAlert = this.alertCtrl.create({
      title: 'Exit',
      subTitle: 'Are you sure?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'                    
        },
        {
          text: 'Yes',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    exitAlert.present();
  }

}