import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiProvider {

  constructor(public http: Http) {}

  getSources() {
    return this.http.get('https://newsapi.org/v1/sources?language=en').map((data) => data.json());
  }

  getArticles(source) {
    let url = `https://newsapi.org/v1/articles?source=${source.id}&apiKey=bb4b99213d39444599aba09a983395f2`;
    return this.http.get(url).map((data) => data.json());
  }

}
