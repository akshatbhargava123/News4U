import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, AlertController } from 'ionic-angular';
import { SocialSharing } from "@ionic-native/social-sharing";

interface Page { 
  title: string;
  icon: string;
  handler: any;
}

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  rootPage: string = 'HomePage';
  pages: Page[];

  constructor(public platform: Platform,
              public navCtrl: NavController,
              public alertCtrl: AlertController,
              public socialSharing: SocialSharing) {
    this.pages = [
      {
        title: 'Home',
        icon: 'home',
        handler: () => {
          this.rootPage = 'HomePage';
        }
      },
      {
        title: 'Share App',
        icon: 'share',
        handler: () => {
          // Share app native code here
          this.socialSharing.share("Check out News4U app, it provides news from more than 60 sources and is very easy to use.");
        }
      },
      {
        title: 'Exit App',
        icon: 'exit',
        handler: () => {
          // Exit app native code here
          this.platform.runBackButtonAction();
        }
      } 
    ];
  }

}