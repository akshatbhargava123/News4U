import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { ApiProvider } from "./../../providers/api/api";
import { Source } from "./../../models/source";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  onlineFetchedSources: Source[] = null;
  sources: Source[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public api: ApiProvider) {
    let dataLoader = this.loadingCtrl.create({
      content: 'Please wait, loading sources...',
      dismissOnPageChange: true
    });
    dataLoader.present();
    this.initItems(dataLoader);
  }

  initItems(dataLoader?: any) {
    if (this.onlineFetchedSources) {
      this.sources = this.onlineFetchedSources;
      return;
    }

    let dataSubscription = this.api.getSources().subscribe((data) => {
      if (dataLoader) dataLoader.dismiss();
      this.sources = data.sources;
      this.onlineFetchedSources = data.sources;
      dataSubscription.unsubscribe();
    });
  }
  
  getItems(ev: any) {
    this.initItems();
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.sources = this.sources.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
    
  }

  openArticlesPage(source) {
    this.navCtrl.push('ArticlesPage', {
      'source': source
    });
  }

}