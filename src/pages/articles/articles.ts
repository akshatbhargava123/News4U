import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { BrowserTab } from "@ionic-native/browser-tab";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from "@ionic-native/social-sharing";

import { ApiProvider } from "./../../providers/api/api";
import { Source } from "./../../models/source";
import { Article } from "./../../models/article";

@IonicPage()
@Component({
  selector: 'page-articles',
  templateUrl: 'articles.html',
})
export class ArticlesPage {

  source: Source;
  articles: Article[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public browsertab: BrowserTab,
              public socialSharing: SocialSharing,
              public inAppBrowser: InAppBrowser,
              public apiProvider: ApiProvider) {
    this.source = this.navParams.data.source;
    let dataLoader = this.loadingCtrl.create({
      content: 'Please wait, loading articles...'
    });
    dataLoader.present();
    this.apiProvider.getArticles(this.source).subscribe((data) => {
      // Wait 1 second for images to load
      setTimeout(() => {
        dataLoader.dismiss();
      }, 1000);
      this.articles = data.articles;
    });
  }

  openInBrowser(article: Article) {
    this.browsertab.isAvailable().then((isAvailable) => {
      if (isAvailable) {
        this.browsertab.openUrl(article.url);
      }
      else {
        this.inAppBrowser.create(article.url).show();
      }
    }).catch((err) => {
      this.inAppBrowser.create(article.url).show();
    })
  }

  share(article: Article) {
    this.socialSharing.share("Check out this news. " + article.title + ". Read in detail @ " + article.url);
  }

  addToWatchLater(article: Article) {
    
  }

}